# Imperative vs Declarative

# I - how to do
# D - what to do

# Logic - Fuzzy Logic

# How many hair do you have to have to be considered not bald?

# "Kind of true kind of false" is the concept of Fuzzy Logic

# Paradigm shifts happen



# Object Oriented Programming & Functional Programming


# OOP

# is a programming paradigm in which we can think about complex problems and objects

# for modeling concrete, real-world things, like cars, as well as relationships between things, like computers and employees, students and teachers, etc...

# OOP models real world entities as software objects that have some data associated with them and can perform certain functions!


def growth_rate(height, time):
    return height / time

tulip = {
    "color": ["pink", "yellow", "purple", "white", "blue"],
    "petals": 3,
    "sepals": 3,
    "environment": "outdoor",
    "stem": True,
    "smell": "a series of terpenes",
    "growthRate": growth_rate,
    "leaves": 3
}

rose = {
    "color": ["pink", "red", "white"],
    "petals": 30,
    "sepals": 3,
    "environment": "outdoor",
    "stem": True,
    "smell": "rosey",
    "growthRate": growth_rate,
    "leaves": 5
}

sunflower = {
    "color": ["yellow", "white"],
    "petals": 45,
    "sepals": 3,
    "environment": "outdoor",
    "stem": True,
    "smell": "sunshine and baseball and earthy",
    "growthRate": growth_rate,
    "leaves": 5
}


# I need a blueprint for a flower

# What are all the necessary conditions to be a flower?

            # color
            # petals
            # sepals
            # env
            # water
            # soil type
            # growth rate
            # leaves
            # stem

# classes are blueprints from which we can create objects
# classes allow us to create custom data types or objects



class PlayerCharacter:
    def __init__(self, name, age):      # dunder method or magic method # runs every time we create a new character
        self.name = name
        self.age = age         # attributes

    def run(self):
        print("run!!!!")
        return "done"



# instantiate a new object useing the class
player1 = PlayerCharacter("Edna", "54")

# print(player1.name)  # player1 is an object created from the CLASS PLAYERCHARACTER

player2 = PlayerCharacter("Julian", "29")
# print(player2.age)

# print(player1.run()) #

# print(help(player1)) # help function shows you everything the object has access to from the class


# Given the class below

# this Cat - name, age - attributes

# Instantiate the Cat with 3 cat objects

# make the cat meow

# make the cat scratch

# make the cat say "I'm hungry"

# feed cat



# Stretch

# Find the oldest cat from all the cats that were instantiated

class Cat:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def meow(self):
        return "meow!"

    def scratch(self):
        return "scratch"

    def hungry(self):
        return "'I'm Hungry'"

    def feed(self, food):
        return f"Cat has been fed {food}"

    def __str__(self): #string method - to print the class in the following way
        return f"The Cat named {self.name} is {self.age} years old."



# cat1 = Cat("Whiskers", 12)
# cat2 = Cat("Meowdy", 4)
# cat3 = Cat("Boots", 7)

# print(cat1.meow())      #output "Meow!"
# print(cat2.scratch())
# print(cat3.hungry())
# print(cat1.feed("fish"))    #output "Cat has been fed fish"

# cats = [cat1.age, cat2.age, cat3.age]

# print(max(cats))


# def get_oldest_cat(*args):
#     return max(args)

# print(f"The oldest cat is {get_oldest_cat(cat1.age, cat1.age, cat3.age)}")

# print(cat1) #output "The Cat named Whiskers is 12 years old.


# Want to track how many dog objects were created
# class Dog:
#     dog_id = 1 # this is a class attribute (everytime a class is created, it will have this attribute) - it is a variable shared across all instances
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
#         self.id = Dog.dog_id
#         Dog.dog_id += 1     # adds 1 every time a new Dog is created

#     def bark(self):
#         pass

#     def __str__(self):
#         return f"Dog {self.id} named {self.name} is {self.age} years old"

#     @classmethod   # decorator function (run this on this function)     #applies to entire class
#     def get_total_dogs(cls):    #checking how many dogs created in this class
#         return cls.dog_id - 1   #-1 because dog_id is already equal to 1


# dog1 = Dog("Spock", 7)
# print(dog1)

# dog2 = Dog("Sniffles", 2)
# print(dog2)

# dog3 = Dog("Towel", 6)
# print(dog3)

# print(Dog.get_total_dogs())



# inheritance - how can we create a class from another class

# class Animal:
#     def __init__(self, animal_type):
#         self.animal_type = animal_type
#     def hunger(self):
#         return "I am hungry"

# class Mammal(Animal):
#     def __init__(self):
#         super().__init__("Mammal")

# dog = Mammal()
# print(dog.hunger())




# Vehicle Exercise
class Vehicle:
    vehicle_id = 1
    def __init__(
        self,
        make,
        model,
        year,
        bodytype,
        color
    ):
        self.make = make
        self.model = model
        self.year = year
        self.bodytype = bodytype
        self.color = color
        Vehicle.vehicle_id += 1

    def turn_on(self):
        return "Car is now on"

    def turn_off(self):
        return "Car is now off"

    def fill_fuel(self):
        return "Low fuel, please fill up"

    def lock_doors(self):
        return "Doors are now locked"

    def windshield_wiper(self):
        return "Whish Wash"

    @classmethod
    def get_total_vehicles(num):
        return num.vehicle_id - 1


vehicle1 = Vehicle("Toyota", "Rav4", 2010, "SUV", "White")
vehicle2 = Vehicle("Honda", "Civic", 2016, "Sedan", "Silver")
vehicle3 = Vehicle("BMW", "335i", 2011, "Sedan", "Silver")

# print(vehicle1.year)

#Find the oldest vehicle

def get_oldest_vehicle(new_dict):
    max_val = 3000
    max_key = None
    for k in new_dict:
        if new_dict[k] < max_val:
            max_val = new_dict[k]
            max_key = k
    return max_key

vehicle_dict = {
    vehicle1.model: vehicle1.year,
    vehicle2.model: vehicle2.year,
    vehicle3.model: vehicle3.year
}

print(f"The oldest vehicle is {get_oldest_vehicle(vehicle_dict)}.")


# track the amount of vehicles instantiated
print(Vehicle.get_total_vehicles())
# output = 3


#Motorcycles subclass inheritance

class Motorcycles(Vehicle):
    def __init__(self, make, model, year, bodytype, color):
        super().__init__(make, model, year, bodytype, color)

motor1 = Motorcycles("Mitsubishi", "Yamaha", 2018, "Motorcycle", "Black")
print(motor1.bodytype)
print(motor1.fill_fuel())
